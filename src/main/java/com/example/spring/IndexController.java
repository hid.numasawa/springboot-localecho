package com.example.spring;

import org.springframework.stereotype.*;
import org.springframework.web.bind.annotation.*;

import reactor.core.publisher.*;

@Controller
@RequestMapping({ "/", "/index" })
public class IndexController {

	@GetMapping
	public String index(@ModelAttribute("form") IndexForm form) {
		return "index";
	}

	@ResponseBody
	@PostMapping
	public Mono<IndexForm> post(@RequestBody IndexForm form) {
		return Mono.just(form);
	};

}
