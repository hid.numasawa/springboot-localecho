package com.example.spring;

import java.util.*;

import com.fasterxml.jackson.annotation.*;

import lombok.*;

@Getter
@Setter
@JsonIgnoreProperties(ignoreUnknown = true)
public class IndexForm {

	String id;
	String text;
	String radio;
	String checkbox;
	String[] multiCheckbox;

	@JsonAnySetter
	Map<String, Object> any;

	@JsonAnyGetter
	public Map<String, Object> getAny() {
		return any;
	}

}
